﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using static Raylib.Raylib;
using static Raylib.Color;
using System.Diagnostics;
using MathClasses;

namespace Project2D
{
    public class SceneObject
    {
        protected SceneObject parent = null;
        protected List<SceneObject> children = new List<SceneObject>();
        protected List<SceneObject> addChildren = new List<SceneObject>();
        protected List<SceneObject> removeChildren = new List<SceneObject>();

        protected Matrix3 localTransform = new Matrix3();
        protected Matrix3 globalTransform = new Matrix3();
        public bool selected = false;
        public MathClasses.Vector2 target = new MathClasses.Vector2();

        public SceneObject() { }
        public SceneObject(string name)
        {
            Init(name);
        }
        public virtual void Init(string name)
        {
            Console.WriteLine("SPWN: SceneObject spawned. " + name);
        }
        ~SceneObject()
        {
            Console.WriteLine("INFO: SceneObject despawned. ");
            if (parent != null)
            {
                parent.RemoveChild(this);
            }
            foreach (SceneObject so in children)
            {
                so.parent = null;
            }
        }
        public int GetChildCount()
        {
            return children.Count;
        }
        public void AddChild(SceneObject child)
        {
            Debug.Assert(child.parent == null);// make sure it doesn't have a parent already
            child.parent = this;
            addChildren.Add(child);
        }
        public void RemoveChild(SceneObject child)
        {
            removeChildren.Add(child);
            child.parent = null;
        }
        public SceneObject GetChild(int index)
        {
            return children[index];
        }
        public SceneObject Parent
        {
            get { return parent; }
        }
        public Matrix3 LocalTransform
        {
            get { return localTransform; }
        }
        public Matrix3 GlobalTransform
        {
            get { return globalTransform; }
        }

        public virtual void OnUpdate(float deltaTime)
        {
        }
        public virtual void OnDraw()
        {
        }

        public void Update(float deltaTime)
        {
            // run OnUpdate behaviour
            OnUpdate(deltaTime);
            // update children
            children.AddRange(addChildren);
            foreach (SceneObject child in removeChildren)
                children.Remove(child);
            addChildren.Clear();
            removeChildren.Clear();

            foreach (SceneObject child in children)
            {
                child.Update(deltaTime);
            }
        }
        public void Draw()
        {
            // run OnDraw behaviour
            OnDraw();
            // draw children
            foreach (SceneObject child in children)
            {
                child.Draw();
            }
        }
        public virtual bool OnClick()
        {
            return false;
        }
        public void UpdateTransform()
        {
            if (parent != null)
                globalTransform = parent.globalTransform * localTransform;
            else
                globalTransform = localTransform;


            foreach (SceneObject child in children)
                child.UpdateTransform();
        }
        public void SetPosition(float x, float y)
        {
            localTransform.SetTranslation(x, y);
            UpdateTransform();
        }
        public void SetRotate(float radians)
        {
            localTransform.SetRotateZ(radians);
            UpdateTransform();
        }
        public void SetScale(float width, float height)
        {
            //localTransform.SetScaled(width, height, 1);
            UpdateTransform();
        }
        public void Translate(float x, float y)
        {
            localTransform.Translate(x, y);
            UpdateTransform();
        }
        public void Rotate(float radians)
        {
            localTransform.RotateZ(radians);
            UpdateTransform();
        }
        public void Scale(float width, float height)
        {
            //localTransform.Scale(width, height, 1);
            UpdateTransform();
        }
        public void SetLocalTransform(Matrix3 m)
        {
            localTransform = m;
            UpdateTransform();
        }

    }
}
