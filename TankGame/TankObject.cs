﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using static Raylib.Raylib;
using static Raylib.Color;
using static Raylib.KeyboardKey;
using static Raylib.MouseButton;

namespace Project2D
{
    class TankObject : SceneObject
    {
        protected List<SceneObject> bullets = new List<SceneObject>();

        public TankObject() { }
        public TankObject(string name)
        {
            Init(name);
        }
        public override void Init(string name)
        {
            Console.WriteLine("SPWN: TankObject spawned. " + name);
        }
        public override void OnUpdate(float deltaTime)
        {
            //Tank movement
            if (IsKeyDown(KEY_W))
                Translate(100 * deltaTime, 0);
            if (IsKeyDown(KEY_S))
                Translate(-100 * deltaTime, 0);
            if (IsKeyDown(KEY_A))
                Rotate(-1 * deltaTime);
            if (IsKeyDown(KEY_D))
                Rotate(1 * deltaTime);

            //Turret movement
            if (children.Count > 1)
            {
                if (IsKeyDown(KEY_Q))
                    children[1].Rotate(-1 * deltaTime);
                if (IsKeyDown(KEY_E))
                    children[1].Rotate(1 * deltaTime);
            }

            //Tank firing
            if (IsKeyPressed(KEY_SPACE))
            {
                Random ran = new Random();
                bullets.Add(new BulletObject());
                Rotate(((float)ran.NextDouble() - 0.5f)*0.1f);
                parent.AddChild(bullets[bullets.Count - 1]);
                bullets[bullets.Count - 1].SetLocalTransform(children[1].GlobalTransform);
                bullets[bullets.Count - 1].Translate(75, -10);
            }

            //AI Move and face towards point
            if (target.x != 0 && target.y != 0)
            {
                MathClasses.Vector2 Heading = target - new MathClasses.Vector2(GlobalTransform.m7, GlobalTransform.m8);
                MathClasses.Vector2 Dir = new MathClasses.Vector2(globalTransform.m1, globalTransform.m2);

                if (Heading.Magnitude() > 250) //stopping range
                    Translate(100 * deltaTime, 0);

                //Handle bearing/direction maths
                Heading.Normalise();
                Dir.Normalise();
                Double Direction = Math.Atan2(Heading.x, Heading.y) - Math.Atan2(Dir.x, Dir.y);
                if ((Direction * (180 / Math.PI) < -1.5 && Direction * (180 / Math.PI) > -180) || Direction * (180 / Math.PI) > 180)
                    Rotate(1 * deltaTime);
                if ((Direction * (180 / Math.PI) > 1.5 && Direction * (180 / Math.PI) < 180) || Direction * (180 / Math.PI) < -180)
                    Rotate(-1 * deltaTime);
                //DrawText((Direction * (180 / Math.PI)).ToString(), (int)GlobalTransform.m7-50, (int)GlobalTransform.m8-80, 20, BLACK);
            }
        }
        public override void OnDraw()
        {
            if (target.x != 0 && target.y != 0)
                DrawRing(new Raylib.Vector2(target.x, target.y), 10, 15, 0, 360, 80, GREEN);

            if (selected)
                DrawRing(new Raylib.Vector2(globalTransform.m7, globalTransform.m8), 60, 65, 0, 360, 80, GREEN);
        }
        public override bool OnClick()
        {
            float Width = 0;
            float Height = 0;
            if (children[0] != null)
            {
                SpriteObject child = (SpriteObject)children[0];
                Width = child.Width;
                Height = child.Height;
            }
            Rectangle rec = new Rectangle(globalTransform.m7 - (Width / 2), globalTransform.m8 - (Height / 2), Width, Height);

            if (CheckCollisionPointRec(GetMousePosition(), rec))
                return true;
            return false;
        }
    }
}
