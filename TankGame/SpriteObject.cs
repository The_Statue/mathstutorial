﻿using Raylib;
using System;
using static Raylib.Raylib;
namespace Project2D
{
    public class SpriteObject : SceneObject
    {
        Texture2D texture = new Texture2D();
        Image image = new Image();
        public float Width
        {
            get { return texture.width; }
        }
        public float Height
        {
            get { return texture.height; }
        }
        public SpriteObject() { }
        public SpriteObject(string name)
        {
            Init(name);
        }
        public override void Init(string name)
        {
            Console.WriteLine("SPWN: SpriteObject spawned. " + name);
        }
        ~SpriteObject()
        {
            Console.WriteLine("INFO: SpriteObject despawned. ");
            //UnloadTexture(texture);
        }
        public void Load(string filename)
        {
            Image img = LoadImage(filename);
            texture = LoadTextureFromImage(img);
            UnloadImage(img);
        }
        public override void OnDraw()
        {
            Color color;

            float rotation = (float)Math.Atan2(globalTransform.m2, globalTransform.m1);
            DrawTextureEx(texture, new Vector2(globalTransform.m7, globalTransform.m8), rotation * (float)(180.0f / Math.PI), 1, Color.white);

        }
    }
}