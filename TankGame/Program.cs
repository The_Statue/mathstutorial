﻿using Raylib;
using System;
using System.Numerics;
using System.Diagnostics;
using static Raylib.Raylib;
using static Raylib.Color;
using static Raylib.CameraType;
using static Raylib.CameraMode;
using static Raylib.KeyboardKey;
using static Raylib.MouseButton;
using static Raylib.MaterialMapType;
using System.Collections.Generic;
using MathClasses;
using Vector3 = Raylib.Vector3;
using Vector2 = Raylib.Vector2;

namespace Project2D
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            SetTargetFPS(60);
            InitWindow(1280, 960, "Tanks for Everything!");
            game.Init();
            while (!WindowShouldClose())
            {
                game.Update();
                game.Draw();
            }
            game.Shutdown();
            CloseWindow();
        }
    }
}
