﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathClasses;
using Raylib;
using static Raylib.Raylib;
using Vector2 = MathClasses.Vector2;

namespace Project2D
{
    class BulletObject : SceneObject
    {
        int force = 100;
        float speed = 250;
        MathClasses.Vector2 screenDimension = new Vector2(GetScreenWidth(), GetScreenHeight());
        public BulletObject()
        {
            SpriteObject bulletTexture = new SpriteObject("bullet texture");
            AddChild(bulletTexture);
            bulletTexture.SetRotate(1.5708f);
            bulletTexture.Load("../Images/Bullets/bulletYellow_outline.png");
        }
        public override void OnUpdate(float deltaTime)
        {
            Translate(speed * deltaTime, 0);
            speed -= force * deltaTime;
            if (speed <= 0 || globalTransform.m7 < 0 || globalTransform.m8 < 0 || globalTransform.m7 > screenDimension.x || globalTransform.m8 > screenDimension.y)
            {
                force = 0;
                parent.RemoveChild(this);
            }

            base.OnUpdate(deltaTime);
        }
    }
}
