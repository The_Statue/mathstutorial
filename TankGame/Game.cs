﻿using System;
using System.Diagnostics;
using Raylib;
using static Raylib.Raylib;
using static Raylib.KeyboardKey;
using static Raylib.MouseButton;
using static Raylib.Color;
using System.Collections.Generic;

namespace Project2D
{
    class Game
    {
        Stopwatch stopwatch = new Stopwatch();
        private long currentTime = 0;
        private long lastTime = 0;
        private float timer = 0;
        private int fps = 1;
        private int frames;
        private float deltaTime = 0.005f;
        protected List<SceneObject> selected = new List<SceneObject>();
        protected List<SceneObject> selectable = new List<SceneObject>();

        SceneObject Root = new SceneObject("root");

        SceneObject tankObject = new TankObject("tank");
        SceneObject turretObject = new SceneObject("turret");
        SpriteObject tankSprite = new SpriteObject("tankS");
        SpriteObject turretSprite = new SpriteObject("turretS");
        SceneObject tankObjectE = new TankObject("tankE");
        SceneObject turretObjectE = new SceneObject("turretE");
        SpriteObject tankSpriteE = new SpriteObject("tankES");
        SpriteObject turretSpriteE = new SpriteObject("turretES");
        Texture2D perlin;

        public void Init()
        {
            stopwatch.Start();
            lastTime = stopwatch.ElapsedMilliseconds;
            tankSprite.Load("../Images/Tanks/tankBlue_outline.png");
            // sprite is facing the wrong way... fix that here
            tankSprite.SetRotate(-90 * (float)(Math.PI / 180.0f));
            // sets an offset for the base, so it rotates around the centre
            tankSprite.SetPosition(-tankSprite.Width / 2.0f, tankSprite.Height / 2.0f);
            turretSprite.Load("../Images/Tanks/barrelBlue.png");
            turretSprite.SetRotate(-90 * (float)(Math.PI / 180.0f));
            // set the turret offset from the tank base
            turretSprite.SetPosition(0, turretSprite.Width / 2.0f);
            
            tankSpriteE.Load("../Images/Tanks/tankRed_outline.png");
            // sprite is facing the wrong way... fix that here
            tankSpriteE.SetRotate(-90 * (float)(Math.PI / 180.0f));
            // sets an offset for the base, so it rotates around the centre
            tankSpriteE.SetPosition(-tankSpriteE.Width / 2.0f, tankSpriteE.Height / 2.0f);
            turretSpriteE.Load("../Images/Tanks/barrelRed.png");
            turretSpriteE.SetRotate(-90 * (float)(Math.PI / 180.0f));
            // set the turret offset from the tank base
            turretSpriteE.SetPosition(0, turretSpriteE.Width / 2.0f);

            // set up the scene object hierarchy - parent the turret to the base,
            // then the base to the tank sceneObject
            Root.AddChild(tankObject);
            Root.AddChild(tankObjectE);

            turretObject.AddChild(turretSprite);
            tankObject.AddChild(tankSprite);
            tankObject.AddChild(turretObject);

            turretObjectE.AddChild(turretSpriteE);
            tankObjectE.AddChild(tankSpriteE);
            tankObjectE.AddChild(turretObjectE);

            // having an empty object for the tank parent means we can set the
            // position/rotation of the tank without
            // affecting the offset of the base sprite
            tankObject.SetPosition(GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f);
            tankObjectE.SetPosition(GetScreenWidth() / 3.0f, GetScreenHeight() / 3.0f);

            Root.Update(deltaTime);
            Root.UpdateTransform();

            selectable.Add(tankObject);
            selectable.Add(tankObjectE);
            Random random = new Random();

            Image test = GenImageColor(2000, 2000, Color.DARKBROWN);
            Image test2 = GenImagePerlinNoise(2000, 2000, random.Next(0,1000), random.Next(0, 1000), 3);
            ImageColorContrast(ref test2, 100);
            ImageColorBrightness(ref test2, 100);
            ImageAlphaMask(ref test, test2);
            ImageAlphaClear(ref test, Color.BROWN, 0.95f);
            ImageAlphaMask(ref test, test2);
            ImageAlphaClear(ref test, Color.DARKGREEN, 0.8f);
            perlin = LoadTextureFromImage(test);
        }
        public void Shutdown()
        { }
        public void Update()
        {
            currentTime = stopwatch.ElapsedMilliseconds;
            deltaTime = (currentTime - lastTime) / 1000.0f;
            timer += deltaTime;
            if (timer >= 1)
            {
                fps = frames;
                frames = 0;
                timer -= 1;
            }
            frames++;
            lastTime = currentTime;

            //Selection module
            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
            {
                bool select = false;
                foreach (SceneObject gameObject in selectable)
                {
                    if (gameObject.OnClick())
                    {
                        selected.Add(gameObject);
                        gameObject.selected = true;
                        select = true;
                    }
                }
                if (!select)
                {
                    if (selected.Count > 0)
                    {
                        foreach (SceneObject gameObject in selected)
                        {
                            gameObject.selected = false;
                            gameObject.target = new MathClasses.Vector2(GetMousePosition().x, GetMousePosition().y);
                        }
                    }
                    selected.Clear();
                }
            }
            if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))
            {
                foreach (SceneObject gameObject in selected)
                { 
                    gameObject.selected = false;
                    gameObject.target = new MathClasses.Vector2(0, 0);
                }
                selected.Clear();
            }
            //Update tank objects
            Root.Update(deltaTime);
        }
        public void Draw()
        {
            BeginDrawing();
            ClearBackground(Color.white);
            DrawTexture(perlin, 00, 00, Color.white);

            Root.Draw();

            DrawText(tankSprite.GlobalTransform.m1 + " " + tankSprite.GlobalTransform.m2 + " " + tankSprite.GlobalTransform.m3, 10, 10, 20, Color.RED);
            DrawText(tankSprite.GlobalTransform.m4 + " " + tankSprite.GlobalTransform.m5 + " " + tankSprite.GlobalTransform.m6, 10, 25, 20, Color.RED);
            DrawText(tankSprite.GlobalTransform.m7 + " " + tankSprite.GlobalTransform.m8 + " " + tankSprite.GlobalTransform.m9, 10, 40, 20, Color.RED);
            EndDrawing();
        }
    }
}
