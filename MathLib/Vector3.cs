﻿using System;
namespace MathClasses
{
    public class Vector3
    {
        public float x, y, z;

        public Vector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3 operator +(Vector3 one, Vector3 two)
        {
            Vector3 sum = new Vector3();

            sum.x = one.x + two.x;
            sum.y = one.y + two.y;
            sum.z = one.z + two.z;

            return sum;
        }

        public static Vector3 operator -(Vector3 one, Vector3 two)
        {
            Vector3 sum = new Vector3();

            sum.x = one.x - two.x;
            sum.y = one.y - two.y;
            sum.z = one.z - two.z;

            return sum;
        }

        public static Vector3 operator *(Vector3 one, float two)
        {
            Vector3 sum = new Vector3();

            sum.x = one.x * two;
            sum.y = one.y * two;
            sum.z = one.z * two;

            return sum;
        }

        public static Vector3 operator *(float one, Vector3 two)
        {
            Vector3 sum = new Vector3();

            sum.x = one * two.x;
            sum.y = one * two.y;
            sum.z = one * two.z;

            return sum;
        }

        public static Vector3 operator *(Matrix3 one, Vector3 two)
        {
            Vector3 sum = new Vector3();

            sum.x = (one.m1 * two.x) + (one.m4 * two.y) + (one.m7 * two.z);
            sum.y = (one.m2 * two.x) + (one.m5 * two.y) + (one.m8 * two.z);
            sum.z = (one.m3 * two.x) + (one.m6 * two.y) + (one.m9 * two.z);

            return sum;
        }

        public float Dot(Vector3 two)
        {
            float sum = 0;

            sum += x * two.x;
            sum += y * two.y;
            sum += z * two.z;

            return sum;
        }

        public Vector3 Cross(Vector3 two)
        {
            Vector3 sum = new Vector3();

            sum.z = (x * two.y)-(y * two.x);
            sum.x = (y * two.z)-(z * two.y);
            sum.y = (z * two.x)-(x * two.z);

            return sum;
        }

        public float Magnitude()
        {
            float sum = (float)Math.Sqrt(x * x + y * y + z * z);

            return sum;
        }

        public float Normalize()
        {
            float sum = (float)Math.Sqrt(x * x + y * y + z * z);

            x /= sum;
            y /= sum;
            z /= sum;

            return sum;
        }

        //
        // NOTE:
        // These implicit operators will convert between a MathClasses.Vector3 & System.Numerics.Vector3
        // This is useful because Raylib has functions that work nicely with System.Numerics.Vector3, 
        // so we can pass our MathClasses.Vector3 directly to Raylib & let the implicit conversions do their job.         
        public static implicit operator Vector3(System.Numerics.Vector3 v)
        {
            return new Vector3 { x = v.X, y = v.Y, z = v.Z };
        }

        public static implicit operator System.Numerics.Vector3(Vector3 v)
        {
            return new System.Numerics.Vector3(v.x, v.y, v.z);
        }
    }
}
