﻿using System;
namespace MathClasses
{
    public class Matrix4
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16;

        public Matrix4()
        {
            m1 =  1; m2 =  0; m3 =  0; m4 =  0;
            m5 =  0; m6 =  1; m7 =  0; m8 =  0;
            m9 =  0; m10 = 0; m11 = 1; m12 = 0;
            m13 = 0; m14 = 0; m15 = 0; m16 = 1;
        }
        public Matrix4(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l, float m, float n, float o, float p)
        {
            m1 =  a; m2 =  b; m3 =  c; m4 =  d; 
            m5 =  e; m6 =  f; m7 =  g; m8 =  h; 
            m9 =  i; m10 = j; m11 = k; m12 = l;
            m13 = m; m14 = n; m15 = o; m16 = p;
        }

        public static Matrix4 operator *(Matrix4 one, Matrix4 two)
        {
            Matrix4 sum = new Matrix4();

            sum.m1  = (one.m1 * two.m1) + (one.m5 * two.m2) + (one.m9  * two.m3) + (one.m13 * two.m4);
            sum.m2  = (one.m2 * two.m1) + (one.m6 * two.m2) + (one.m10 * two.m3) + (one.m14 * two.m4);
            sum.m3  = (one.m3 * two.m1) + (one.m7 * two.m2) + (one.m11 * two.m3) + (one.m15 * two.m4);
            sum.m4  = (one.m4 * two.m1) + (one.m8 * two.m2) + (one.m12 * two.m3) + (one.m16 * two.m4);
                   
            sum.m5  = (one.m1 * two.m5) + (one.m5 * two.m6) + (one.m9  * two.m7) + (one.m13 * two.m8);
            sum.m6  = (one.m2 * two.m5) + (one.m6 * two.m6) + (one.m10 * two.m7) + (one.m14 * two.m8);
            sum.m7  = (one.m3 * two.m5) + (one.m7 * two.m6) + (one.m11 * two.m7) + (one.m15 * two.m8);
            sum.m8  = (one.m4 * two.m5) + (one.m8 * two.m6) + (one.m12 * two.m7) + (one.m16 * two.m8);
                   
            sum.m9  = (one.m1 * two.m9) + (one.m5 * two.m10) + (one.m9  * two.m11) + (one.m13 * two.m12);
            sum.m10 = (one.m2 * two.m9) + (one.m6 * two.m10) + (one.m10 * two.m11) + (one.m14 * two.m12);
            sum.m11 = (one.m3 * two.m9) + (one.m7 * two.m10) + (one.m11 * two.m11) + (one.m15 * two.m12);
            sum.m12 = (one.m4 * two.m9) + (one.m8 * two.m10) + (one.m12 * two.m11) + (one.m16 * two.m12);

            sum.m13 = (one.m1 * two.m13) + (one.m5 * two.m14) + (one.m9  * two.m15) + (one.m13 * two.m16);
            sum.m14 = (one.m2 * two.m13) + (one.m6 * two.m14) + (one.m10 * two.m15) + (one.m14 * two.m16);
            sum.m15 = (one.m3 * two.m13) + (one.m7 * two.m14) + (one.m11 * two.m15) + (one.m15 * two.m16);
            sum.m16 = (one.m4 * two.m13) + (one.m8 * two.m14) + (one.m12 * two.m15) + (one.m16 * two.m16);

            return sum;
        }
        public void SetRotateX(float angle)
        {
            m1 =  1; m2 =   0;                       m3 = 0;                         m4 = 0;
            m5 =  0; m6 =   (float)Math.Cos(angle);  m7 = (float)Math.Sin(angle);    m8 = 0;
            m9 =  0; m10 = -(float)Math.Sin(angle);  m11 = (float)Math.Cos(angle);   m12 = 0;
            m13 = 0; m14 =  0;                       m15 = 0;                        m16 = 1;
        }
        public void SetRotateY(float angle)
        {
            m1 =  (float)Math.Cos(angle);   m2 =  0;    m3 = -(float)Math.Sin(angle);   m4 = 0;
            m5 =  0;                        m6 = 1;     m7 =  0;                        m8 = 0;
            m9 =  (float)Math.Sin(angle);   m10 = 0;    m11 = (float)Math.Cos(angle);   m12 = 0;
            m13 = 0;                        m14 = 0;    m15 = 0;                        m16 = 1;
        }
        public void SetRotateZ(float angle)
        {
            m1 =  (float)Math.Cos(angle);   m2 = (float)Math.Sin(angle);    m3 = 0; m4 = 0;
            m5 = -(float)Math.Sin(angle);   m6 = (float)Math.Cos(angle);    m7 = 0; m8 = 0;
            m9 =  0;                        m10 = 0;                        m11 = 1; m12 = 0;
            m13 = 0;                        m14 = 0;                        m15 = 0; m16 = 1;
        }
    }
}
